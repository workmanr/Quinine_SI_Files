structure      quinine.2+.ATx22.solv.ion.psf                # connectivity of system
coordinates    quinine.2+.ATx22.solv.ion.pdb                # initial coordinates
binCoordinates quinine.2+.ATx22.solv.ion.prod.4.coor        # restart coordinates
binVelocities  quinine.2+.ATx22.solv.ion.prod.4.vel         # restart velocities
extendedSystem quinine.2+.ATx22.solv.ion.prod.4.xsc         # restart PBC parameters

paraTypeCharmm on                                           # use CHARMM type force field
parameters     quinine.2+.avogadro.cgenff.par               # parameters for non-CGENFF quinine atom types
parameters     par_all36_na.prm                             # parameters for DNA/RNA
parameters     par_all36_cgenff.prm                         # parameters for CGENFF atom types
parameters     water_ions.par                               # parameters for water/ions
set temp       300                                          # temperature variable (NOT A NAMD ARGUMENT)
#temperature    $temp                                       # if no velocities, sets temperature, commented out here

timestep       2.0                                          # 2 fs timestep
rigidBonds     all                                          # heavy atom-hydrogen bonds are rigid
stepspercycle  10                                           # steps between electrostatic force evaluation
exclude        scaled1-4                                    # 1-3 forces are ignored and some 1-4 interactions modified
cutoff         10.0                                         # nonbonded force for pairs with distance < cutoff calculated
switching      on                                           # LJ/electrostatic force smoothed to 0 from switchdist to cutoff
switchdist     8.0                                          # beginning of nonbonded force smoothing function
pairlistdist   12.0                                         # atom pairs with distance < pairlistdist included in pairlists
PME            on                                           # particle Mesh Ewald for full-system electrostatics
PMEGridSpacing 1.0                                          # grid size for PME algorithm

constraints on                                              # harmonic restraints
consref quinine.2+.ATx22.solv.ion.term.ref                  # pdb file specifying atoms to be restrained and positions
conskfile quinine.2+.ATx22.solv.ion.term.k.ref              # pdb file containing force constants to restrain with
conskcol B                                                  # beta-column used for consref and conskref info

langevin         on                                         # Langevin thermostat for temperature control
langevinDamping  1                                          # damping constant gamma
langevinTemp     $temp                                      # target temperature in K
langevinHydrogen off                                        # hydrogen atoms ignored for Langevin dynamics

useGroupPressure     yes                                    # hydrogen group based virial used for pressure/KE evaluation
langevinPiston       on                                     # Langevin piston barostat active
langevinPistonTarget 1.01325                                # target pressure in bar
langevinPistonPeriod 100.0                                  # barostat oscillation time scale in fs
langevinPistonDecay  100.0                                  # barostat damping time scale in fs
langevinPistonTemp   $temp                                  # barostat temperature, set same as langevinTemp

#cellBasisVector1     68.0  0.0   0.0                       # x-axis PBC length, restart parameters used here, commented
#cellBasisVector2      0.0 68.0   0.0                       # y-axis PBC length, restart parameters used here, commented
#cellBasisVector3      0.0  0.0  82.0                       # z-axis PBC length, restart parameters used here, commented
#cellOrigin            0.0  0.0   0.0                       # xyz PBC center, restart parameters used here, commented
wrapAll               on                                    # consider PBC when writing coordinates

colvars       on                                            # collective variable module
colvarsConfig restraint.cylinder.colvar                     # collective variable configuration file

outputName     quinine.2+.ATx22.solv.ion.prod.5             # output file prefix
restartfreq    1000                                         # how often to write restart files
dcdfreq        1000                                         # how often to write coordinate trajectory
outputEnergies 1000                                         # how often to write energies to log
outputPressure 1000                                         # how often to write pressures to log

run 10000000                                                # run for 10 million steps = 20 fs
