import file_utils as fil                      # import file reading functions
import vector_utils as vect                   # import vector functions

prefix = "quinine.2+.ATx22.rerun."                                      # data file prefix
atoms = ["C1", "C2", "C3", "C4", "C5", "C6", "C7", "C8", "C9", "C10"]   # quinoline atom names
suffix = ".pair.dat"                                                    # data file suffix

Fdata = []                                                            # empty data array to fill
for atom in atoms:                                                    # loop over quinoline atom names
 Fdata.append(fil.Read_Data(prefix+atom+suffix))                     # index data file as xyz array in Fdata
vectors = fil.Read_Data("aligned.dD.vectors.dat")                     # xyz array of dipole derivative (dD) vectors
frames = 4000                                                         # number of data frames
N = 10                                                                # number of atoms
q = [-0.064, 0.153, 0.014, 0.104, 0.133, -0.113, -0.051, -0.112, 0.22, -0.229]    # charges of quinoline atoms
unitconv = 1.0365*4.184                                               # convert kcal/(mol*A*q_au) to MV/cm

for i in range(frames):                                       # loop over data frames
 for j in range(N):                                          # for each frame loop over atoms
   vect.Scalar_Product(Fdata[j][i], q[j])                    # product of force vectors and 1/charge for each atom

Esum = 0.0                                                    # instantiate sum of E for all frames

for i in range(frames):                                       # loop over data frames
 Estepsum = 0.0                                              # instantiate sum of E per data frame
 for j in range(N):                                          # for each frame loop over atoms
   Estepsum += vect.Dot(Fdata[j][i], vectors[i])             # project forces onto dD vectors and add to frame E sum
 Esum += Estepsum/N                                          # add average E per frame to total E sum
Eavg = (Esum/steps)*unitconv                                  # average total E over number of frames and convert units
print("Mean local E-field = "+str(Eavg)+" MV/cm")             # print final E value
