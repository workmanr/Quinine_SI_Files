#!/bin/bash
prefix="quinine.2+.ATx22.rerun."
atoms=(C1 C2 C3 C4 C5 C6 C7 C8 C9 N1)
for atom in ${atoms[@]};
do
 logfile=$prefix$atom.log
 datafile=$prefix$atom.pair.dat
 grep "PAIR INTERACTION:" $logfile | awk '{print $10,$11,$12}' > $datafile
done
